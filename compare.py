# Création des deux ensembles de dictionnaires pour les périodes 1 et 2

periode_1 = [
    {"company_label": "Microsoft", "nb_users": 100000, "total_amount": 5000000},
    {"company_label": "Google", "nb_users": 120000, "total_amount": 6000000},
    {"company_label": "Amazon", "nb_users": 80000, "total_amount": 4000000}
]

periode_2 = [
    {"company_label": "Microsoft", "nb_users": 110000, "total_amount": 5500000},
    {"company_label": "Google", "nb_users": 125000, "total_amount": 6250000},
    {"company_label": "Apple", "nb_users": 90000, "total_amount": 4500000}
]

import pandas as pd

# Convertir les ensembles de dictionnaires en DataFrames Pandas
df_periode_1 = pd.DataFrame(periode_1)
df_periode_2 = pd.DataFrame(periode_2)

# Fusionner les DataFrames sur la base de 'company_label'
df_fusion = pd.merge(df_periode_1, df_periode_2, on="company_label", how="outer", suffixes=('_p1', '_p2'))

# Remplir les valeurs manquantes par 0
df_fusion.fillna(0, inplace=True)

# Calculer la différence et le pourcentage de changement pour 'nb_users' et 'total_amount'
df_fusion['diff_nb_users'] = df_fusion['nb_users_p2'] - df_fusion['nb_users_p1']
df_fusion['diff_total_amount'] = df_fusion['total_amount_p2'] - df_fusion['total_amount_p1']

df_fusion['pct_change_nb_users'] = df_fusion['diff_nb_users'] / df_fusion['nb_users_p1'].replace(0, 1) * 100
df_fusion['pct_change_total_amount'] = df_fusion['diff_total_amount'] / df_fusion['total_amount_p1'].replace(0, 1) * 100

# Sélectionner et organiser les colonnes pour le DataFrame final
colonnes_finale = [
  'company_label',
  'nb_users_p1',
  'total_amount_p1',
  'nb_users_p2',
  'total_amount_p2',
  'diff_nb_users',
  'pct_change_nb_users',
  'diff_total_amount',
  'pct_change_total_amount'
]

df_comparatif = df_fusion[colonnes_finale]

df_comparatif

# WITHOUT PANDAS

# Fusionner les dictionnaires et calculer les différences et pourcentages sans utiliser Pandas

# Créer un dictionnaire combiné pour la fusion
fusion_dict = {}
for d in periode_1 + periode_2:
    label = d['company_label']
    if label not in fusion_dict:
        fusion_dict[label] = {'nb_users_p1': 0, 'total_amount_p1': 0, 'nb_users_p2': 0, 'total_amount_p2': 0}
    if d in periode_1:
        fusion_dict[label]['nb_users_p1'] = d['nb_users']
        fusion_dict[label]['total_amount_p1'] = d['total_amount']
    else:
        fusion_dict[label]['nb_users_p2'] = d['nb_users']
        fusion_dict[label]['total_amount_p2'] = d['total_amount']

# Calculer la différence et le pourcentage de changement
for label, data in fusion_dict.items():
    data['diff_nb_users'] = data['nb_users_p2'] - data['nb_users_p1']
    data['diff_total_amount'] = data['total_amount_p2'] - data['total_amount_p1']
    data['pct_change_nb_users'] = (data['diff_nb_users'] / (data['nb_users_p1'] or 1)) * 100
    data['pct_change_total_amount'] = (data['diff_total_amount'] / (data['total_amount_p1'] or 1)) * 100

# Préparer les données pour l'affichage
resultat_final = []
for label, data in fusion_dict.items():
    resultat_final.append({
        'company_label': label,
        **data
    })

# Afficher le résultat
resultat_final




