from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

from typing import Any, Dict, List, Optional, Union

from pydantic import BaseModel, model_validator

app = FastAPI()

templates = Jinja2Templates(directory="templates")

class Table(BaseModel):
    headers: list[dict]
    rows: list[dict]

    @model_validator(mode="after")
    def check_nb_columns(self):
        headers = self.headers
        rows = self.rows
 
        nb_columns = len(headers)

        for row in rows:
            if len(row) != nb_columns:
                raise ValueError("rows must have as many elements as there are columns")
        else:
            return self

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "headers": ["Nom", "Prénom", "Date de naissance"],
                    "rows": [
                        ["Mirabel", "Paul", "29/11/1995"],
                        ["Khojandi", "Kyan", "29/08/1982"],
                        ["Niney", "Pierre", "13/03/1989"],
                    ],
                }
            ]
        }
    }

class Header(BaseModel):
    title: Optional[str] = None
    rowspan: Optional[int] = 1
    colspan: Optional[int] = 1
    position: Optional[int] = -1  # Position de l'en-tête dans le tableau

class TableWithSubheaders(BaseModel):
    total_columns: int  # Nombre total de colonnes du tableau
    headers: List[Header]
    subheaders: List[Header]
    rows: List[Dict[str, Any]]

    @model_validator(mode="after")
    def check_nb_columns(self):
        subheaders = self.subheaders
        rows = self.rows

        nb_columns = len(subheaders)

        for row in rows:
            if len(row) != nb_columns:
                raise ValueError("rows must have as many elements as there are columns")
        else:
            return self

    # model_config = {
    #     "json_schema_extra": {
    #         "examples": [
    #             {
    #                 "headers": [{}, {"title": "du 12/02/23 au 15/02/23", "rowspan": 2, "colspan": 2}"Prénom", "Date de naissance"],
    #                 "subheaders": ["Nom", "Prénom", "Date de naissance"],
    #                 "rows": [
    #                     ["Mirabel", "Paul", "29/11/1995"],
    #                     ["Khojandi", "Kyan", "29/08/1982"],
    #                     ["Niney", "Pierre", "13/03/1989"],
    #                 ],
    #             }
    #         ]
    #     }
    # }
        

@app.get("/")
async def home(request: Request):
    return templates.TemplateResponse("home.html.jinja2", {"request": request})

@app.post("/generate-table", response_class=HTMLResponse)
async def generate_table(request: Request, table: Table):
    print(table)
    return templates.TemplateResponse("partials/table.html.jinja2", {"request": request, "table": table})

@app.post("/generate-table-with-subheaders", response_class=HTMLResponse)
async def generate_table_with_subheaders(request: Request, table: TableWithSubheaders):
    return templates.TemplateResponse("partials/table_with_subheaders.html.jinja2", {"request": request, "table": table})